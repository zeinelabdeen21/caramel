import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'app_theme.dart';

class AuthTextAccount extends StatelessWidget {
  final String text;
  final String title;
  final VoidCallback onTap;
  const AuthTextAccount(
      {Key? key, required this.text, required this.title, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          text,
          style: const TextStyle(
            color: Colors.black,
            fontSize: 14,
            fontWeight: FontWeight.w400,
          ),
        ).tr(),
        SizedBox(
          width: 4.w,
        ),
        InkWell(
          onTap: onTap,
          child: Text(
            title,
            style: TextStyle(
              color: AppTheme.mainColor,
              fontSize: 14,
              fontWeight: FontWeight.w900,
            ),
          ).tr(),
        ),
      ],
    );
  }
}

////////////////////
///
///   LOGO
///
//////////////////

class CustomLogo extends StatelessWidget {
  const CustomLogo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Image.asset(
        'assets/icons/splash_icon.png',
        width: 129.w,
      ),
    );
  }
}

////////////////////
///
///   customTitle
///
//////////////////

class CustomTitle extends StatelessWidget {
  final String text;
  final String title;
  final VoidCallback onTap;
  const CustomTitle(
      {Key? key, required this.text, required this.title, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(15, 0, 15, 0),
      child: Row(
        children: [
          Text(
            title,
            style: const TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              color: Color(0xFF000000),
            ),
          ).tr(),
          const Spacer(),
          InkWell(
            onTap: onTap,
            child: Text(
              text,
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
                color: AppTheme.mainColor,
              ),
            ).tr(),
          ),
        ],
      ),
    );
  }
}

class CustomOnlyTitle extends StatelessWidget {
  final String title;
  const CustomOnlyTitle({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(15, 0, 15, 0),
      child: Text(
        title,
        style: const TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w900,
          color: Colors.black,
        ),
      ).tr(),
    );
  }
}
