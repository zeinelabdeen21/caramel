import 'package:flutter/material.dart';

Widget lengthTetx({
  BuildContext? contaxt,
  dynamic txt,
  int? nu,
}) {
  return Text(
    txt.length > nu ? txt.substring(0, nu) + "...." : txt,
    style: const TextStyle(
      fontSize: 12,
      fontWeight: FontWeight.w700,
      color: Colors.black,
    ),
  );
}
