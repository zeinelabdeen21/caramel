import 'package:kiwi/kiwi.dart';

import '../screens/navigation/bloc/bloc_bloc.dart';
import '../screens/sections/bloc/bloc.dart';

void KiwiInit() {
  KiwiContainer container = KiwiContainer();

  container.registerFactory((c) => NavigationRailBloc());
  container.registerFactory((c) => NavigationBloc());
}
