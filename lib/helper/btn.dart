import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'app_theme.dart';

class Btn extends StatelessWidget {
  final String txt;
  final VoidCallback onTap;
  final double? width;
  final double? height;
  final Color? color;
  final Color? colorText;

  const Btn(
      {super.key,
      required this.txt,
      required this.onTap,
      this.width,
      this.height,
      this.color,
      this.colorText});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 15, left: 15, top: 15, bottom: 15),
      child: InkWell(
        onTap: onTap,
        hoverColor: Colors.white,
        child: Container(
          alignment: Alignment.center,
          width: width ?? MediaQuery.of(context).size.width,
          height: height ?? 50,
          decoration: BoxDecoration(
            // color: _pressed ? AppTheme.primaryColor : Colors.indigo,
            color: color ?? AppTheme.mainColor,
            borderRadius: BorderRadius.circular(8),
            // border: Border.all(
            //   width: 0.5,
            //   color: Colors.grey,
            // ),
          ),
          child: Text(
            txt,
            style: TextStyle(
              color: colorText ?? Colors.white,
              fontSize: 14,
              fontWeight: FontWeight.w800,
            ),
          ).tr(),
        ),
      ),
    );
  }
}

class BtnOut extends StatelessWidget {
  final String txt;
  final VoidCallback onTap;
  final double? width;
  final double? height;
  final Color? color;
  final Color? colorText;

  const BtnOut(
      {super.key,
      required this.txt,
      required this.onTap,
      this.width,
      this.height,
      this.color,
      this.colorText});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      hoverColor: Colors.white,
      child: Container(
        alignment: Alignment.center,
        width: width ?? MediaQuery.of(context).size.width,
        height: height ?? 50,
        decoration: BoxDecoration(
          // color: _pressed ? AppTheme.primaryColor : Colors.indigo,
          color: color ?? Colors.red,
          borderRadius: BorderRadius.circular(8),
          // border: Border.all(
          //   width: 0.5,
          //   color: Colors.grey,
          // ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(
              Icons.login,
              size: 25,
              color: Colors.white,
            ),
            SizedBox(
              width: 8.w,
            ),
            Text(
              txt,
              style: TextStyle(
                color: colorText ?? Colors.white,
                fontSize: 14,
                fontWeight: FontWeight.w800,
              ),
            ).tr(),
          ],
        ),
      ),
    );
  }
}

class BtnAddCart extends StatelessWidget {
  final String txt;
  final VoidCallback onTap;

  const BtnAddCart({super.key, required this.txt, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 15, left: 15, top: 15, bottom: 15),
      child: InkWell(
        onTap: onTap,
        hoverColor: Colors.white,
        child: Container(
          alignment: Alignment.center,
          width: double.infinity,
          height: 38,
          decoration: BoxDecoration(
            // color: _pressed ? AppTheme.primaryColor : Colors.indigo,
            // color: AppTheme.mainColor,
            borderRadius: BorderRadius.circular(9),
            border: Border.all(
              width: 0.5,
              color: AppTheme.mainColor,
            ),
          ),
          child: Text(
            txt,
            style: TextStyle(
              color: AppTheme.mainColor,
              fontSize: 11,
              fontWeight: FontWeight.w800,
            ),
          ).tr(),
        ),
      ),
    );
  }
}

class Btn2 extends StatelessWidget {
  final String txt;
  final VoidCallback onTap;
  const Btn2({Key? key, required this.txt, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 15, left: 15, top: 15, bottom: 15),
      child: InkWell(
        onTap: onTap,
        hoverColor: Colors.white,
        child: Container(
          alignment: Alignment.center,
          width: double.infinity,
          height: 45,
          decoration: BoxDecoration(
            // color: _pressed ? AppTheme.primaryColor : Colors.indigo,
            color: AppTheme.mainColor,
            borderRadius: BorderRadius.circular(15),
            border: Border.all(
              width: 0.5,
              color: Colors.grey,
            ),
          ),
          child: Text(
            txt,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 12,
              fontWeight: FontWeight.w800,
            ),
          ).tr(),
        ),
      ),
    );
  }
}

class BtnBorder extends StatelessWidget {
  final String txt;
  final VoidCallback onTap;
  const BtnBorder({Key? key, required this.txt, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 15, left: 15, top: 15, bottom: 15),
      child: InkWell(
        onTap: onTap,
        hoverColor: Colors.white,
        child: Container(
          alignment: Alignment.center,
          width: double.infinity,
          height: 45,
          decoration: BoxDecoration(
            // color: _pressed ? AppTheme.primaryColor : Colors.indigo,
            // color: AppTheme.mainColor,
            borderRadius: BorderRadius.circular(15),
            border: Border.all(
              width: 0.5,
              color: AppTheme.mainColor,
            ),
          ),
          child: Text(
            txt,
            style: TextStyle(
              color: AppTheme.mainColor,
              fontSize: 12,
              fontWeight: FontWeight.w800,
            ),
          ).tr(),
        ),
      ),
    );
  }
}
