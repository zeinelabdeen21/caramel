import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppTheme {
  AppTheme._();
  static Color mainColor = const Color(0xFFF85F09);
  static Color backgroundScreen = const Color(0xFFFBF6F0);

  static TextStyle titleWelcom = TextStyle(
    fontSize: 14,
    color: Colors.black,
    height: 1.5.h,
    fontWeight: FontWeight.bold,
  );

  static TextStyle title = TextStyle(
    fontSize: 14,
    color: Colors.black,
    height: 1.5.h,
    fontWeight: FontWeight.bold,
  );

  static TextStyle hintText = TextStyle(
    fontSize: 12,
    color: Colors.black,
    height: 1.5.h,
    fontWeight: FontWeight.w500,
  );

  static SizedBox sizedBoxH = SizedBox(
    height: 20.h,
  );

  static SizedBox sizedBoxtextForm = SizedBox(
    height: 12.h,
  );
}
