import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';

class SliderCustom extends StatelessWidget {
  const SliderCustom({super.key});

  @override
  Widget build(BuildContext context) {
    return Swiper(
      itemBuilder: (BuildContext context, index) {
        return Padding(
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(18),
            child: Image.network(
              "https://www.caramelbb.com/image/cache/catalog/banners/one/1675924724bodymist-default.png",
              fit: BoxFit.fill,
              width: MediaQuery.of(context).size.width.w,
              height: MediaQuery.of(context).size.height.h,
            ),
          ),
        );
      },
      autoplay: true,
      itemCount: 5,
      scrollDirection: Axis.horizontal,
      pagination: SwiperPagination(
        alignment: Alignment.bottomCenter,
        builder: DotSwiperPaginationBuilder(
          color: Colors.white.withOpacity(0.5),
          activeColor: Colors.white,
        ),
        margin: const EdgeInsets.only(
          top: 90,
        ),
      ),
    );
  }
}
