import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../assets.dart';
import '../../../generated/locale_keys.g.dart';
import '../../../helper/btn.dart';
import '../../../helper/text.dart';

class CustomBrand extends StatelessWidget {
  final String? img;
  final String? txt;
  const CustomBrand({super.key, this.img, this.txt});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 50,
          height: 50,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: const Color(0xFFFBF6F0),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Image.network(
            // 'assets/icons/slider_img.png',
            img ?? "",
            width: 25.w,

            fit: BoxFit.fill,
          ),
        ),
        SizedBox(
          height: 5.h,
        ),
        Text(
          // "عروض الشتاء",
          txt ?? "",
          style: TextStyle(
            fontFamily: Assets.fonts.jfFlatRegularTTF,
            fontSize: 12,
            fontWeight: FontWeight.w800,
            color: Colors.black,
          ),
        ),
      ],
    );
  }
}

///// cartBox
class CartBox extends StatelessWidget {
  final String? iconFav;
  final String? image;
  final String? descont;
  final String? pres;
  final String? txt;
  final dynamic ontapFav;
  final dynamic ontapPage;
  final dynamic onTapCart;
  final dynamic addCart;
  final String textAddCart;
  const CartBox(
      {super.key,
      this.iconFav,
      this.image,
      this.descont,
      this.pres,
      this.txt,
      this.ontapFav,
      this.ontapPage,
      this.onTapCart,
      this.addCart,
      required this.textAddCart});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          width: 1,
          color: Colors.grey.withOpacity(0.3),
        ),
      ),
      height: 306,
      width: 180,
      child: Column(
        children: [
          SizedBox(
            height: 150,
            child: Stack(
              children: [
                InkWell(
                  onTap: ontapPage,
                  // onTap: () {
                  // navigateTo(
                  //   context,
                  //   ProductScreen(),
                  // );
                  // },
                  child: Image(
                    // image: AssetImage(
                    //   'assets/icons/shos.png',
                    // ),
                    image: NetworkImage(
                      image ?? "",
                    ),
                    width: 180,
                    fit: BoxFit.fill,
                  ),
                ),
                Align(
                  alignment: context.locale.languageCode == "ar"
                      ? Alignment.topLeft
                      : Alignment.topRight,
                  child: Padding(
                    padding: const EdgeInsets.all(8),
                    child: Container(
                      alignment: Alignment.center,
                      width: 30,
                      height: 30,
                      padding: const EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(100),
                      ),
                      child: InkWell(
                        onTap: ontapFav,
                        child: Image.asset(
                          Assets.icons.heartCPNG,
                          width: 25.w,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 10,
              right: 10,
              top: 20,
            ),
            child: Column(
              children: [
                lengthTetx(
                  nu: 30,
                  txt: txt,
                  contaxt: context,
                ),
                Row(
                  children: [
                    Row(
                      children: [
                        Text(
                          // '600.50',
                          descont ?? "",
                          style: const TextStyle(
                            decoration: TextDecoration.lineThrough,
                            decorationColor: Colors.black,
                            decorationThickness: 4,
                            fontSize: 12,
                            fontWeight: FontWeight.w700,
                            color: Colors.black,
                          ),
                        ),
                        SizedBox(
                          width: 5.w,
                        ),
                        Text(
                          tr(LocaleKeys.BoxCatogry_RS),
                          style: const TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w700,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 8.w,
                    ),
                    Row(
                      children: [
                        Text(
                          pres ?? "",
                          style: const TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w900,
                            color: Colors.red,
                          ),
                        ),
                        SizedBox(
                          width: 5.w,
                        ),
                        Text(
                          tr(LocaleKeys.BoxCatogry_RS),
                          style: const TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w900,
                            color: Colors.red,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.h,
                ),
                // Text(
                //   txt ?? "",
                //   style: TextStyle(
                //       fontFamily: Family.regular,
                //       fontSize: 14,
                //       fontWeight: FontWeight.w900,
                //       color: BottonNavCubit.get(context).isDark
                //           ? Colors.white
                //           : Colors.black,
                //       height: 1.5),
                // ),
              ],
            ),
          ),
          BtnAddCart(
            txt: textAddCart,
            onTap: addCart,
          ),
        ],
      ),
    );
  }
}

// Text mor
Widget textMore({
  BuildContext? context,
  String? txt,
  String? ontapText,
  dynamic color,
  dynamic ontap,
}) {
  return Padding(
    padding: const EdgeInsets.only(
      left: 15,
      right: 15,
      // top: 20,
      bottom: 20,
    ),
    child: Row(
      children: [
        Text(
          // 'أطقم متناسقة',
          txt ?? "",
          style: TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.w900,
            color: color ?? Colors.black,
          ),
        ),
        const Spacer(),
        InkWell(
          onTap: ontap,
          child: Text(
            // 'عرض الكل',
            ontapText ?? "",
            style: TextStyle(
              fontSize: 13,
              fontWeight: FontWeight.w500,
              color: color ?? const Color(0xFF9A9A9A),
            ),
          ),
        ),
      ],
    ),
  );
}
