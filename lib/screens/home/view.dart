import 'package:caramelbb/helper/app_theme.dart';
import 'package:caramelbb/screens/home/widget/custom_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'widget/custom_brand.dart';

class HomeView extends StatelessWidget {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        AppTheme.sizedBoxH,
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 164.h,
          child: const SliderCustom(),
        ),
        AppTheme.sizedBoxH,
        Container(
          color: Colors.white,
          width: double.infinity,
          child: GridView.count(
            padding: const EdgeInsets.all(15.0),
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            // crossAxisCount: size > 300 ? 2 : size > 500 ? 5 : size > 600 ? 8 : 9,
            crossAxisCount: 4,
            // crossAxisCount: mmm(context),
            mainAxisSpacing: 10.0,
            crossAxisSpacing: 10.0,
            childAspectRatio: 1 / 1,
            children: List.generate(
              8,
              (index) {
                return const CustomBrand(
                  img:
                      "https://www.caramelbb.com/image/cache/catalog/icons/1656231921perfume-default.png",
                  txt: "اسم البراند",
                );
              },
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.only(
            top: 20,
            bottom: 30,
          ),
          child: Column(
            children: [
              textMore(
                context: context,
                txt: 'أطقم متناسقة',
                ontapText: 'عرض الكل',
                ontap: () {},
                color: Colors.black,
              ),
              Container(
                height: 300,
                child: ListView.builder(
                  shrinkWrap: true,
                  // physics: NeverScrollableScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  itemCount: 6,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.only(
                        left: 15,
                        right: 15,
                      ),
                      child: CartBox(
                        onTapCart: () {},
                        ontapFav: () {},
                        ontapPage: () {},
                        txt:
                            "سنيكرز برو ليزر مصنوع من الجلد الفائق من تشكيلة كيث هارينج أبيض/أسود",
                        image:
                            "https://www.caramelbb.com/image/cache/catalog/products/diffuser%20/1675680481Diffuser-11-200x200.png",
                        descont: "494.44",
                        pres: "494.44",
                        textAddCart: "اضف للسلة",
                        addCart: () {},
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
