import 'package:caramelbb/screens/navigation/bloc/bloc_event.dart';
import 'package:caramelbb/screens/navigation/bloc/bloc_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NavigationBloc extends Bloc<NavigationEvent, NavigationState> {
  // NavigationBloc() : super(NavigationState.initial());
  NavigationBloc() : super(NavigationState.initial()) {
    on<UpdateNavigationIndex>(_getData);
  }

  void _getData(
    NavigationEvent event,
    Emitter<NavigationState> emit,
  ) async {
    if (event is UpdateNavigationIndex) {
      emit(NavigationState(currentIndex: event.newIndex));
    }
  }

  // @override
  // Stream<NavigationState> mapEventToState(NavigationEvent event) async* {
  //   if (event is UpdateNavigationIndex) {
  //     yield NavigationState(currentIndex: event.newIndex);
  //   }
  // }
}
