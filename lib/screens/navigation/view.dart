import 'package:caramelbb/screens/navigation/bloc/bloc_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kiwi/kiwi.dart';

import '../../assets.dart';
import '../../helper/app_theme.dart';
import '../cart/view.dart';
import '../home/view.dart';
import '../my_profile/view.dart';
import '../offers/view.dart';
import '../sections/view.dart';
import 'bloc/bloc_bloc.dart';
import 'bloc/bloc_state.dart';

class NavigationView extends StatefulWidget {
  const NavigationView({super.key});

  @override
  State<NavigationView> createState() => _NavigationViewState();
}

class _NavigationViewState extends State<NavigationView> {
  final NavigationBloc _bloc = KiwiContainer().resolve()
    ..add(UpdateNavigationIndex(newIndex: 0));
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _bloc.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFBF6F0),
      appBar: AppBar(
        title: Image.asset(
          Assets.icons.logoPNG,
          width: 120.w,
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Image.asset(
              Assets.icons.searchPNG,
              width: 25.w,
            ),
          ),
          IconButton(
            iconSize: 0,
            onPressed: () {},
            icon: Image.asset(
              Assets.icons.heartPNG,
              width: 25.w,
            ),
          ),
        ],
      ),
      body: BlocBuilder<NavigationBloc, NavigationState>(
        bloc: _bloc,
        builder: (context, state) {
          return [
            const HomeView(),
            const SectionsView(),
            const OffersView(),
            const CartView(),
            const MyProfile(),
          ][state.currentIndex];
        },
      ),
      bottomNavigationBar: BlocBuilder<NavigationBloc, NavigationState>(
        bloc: _bloc,
        builder: (context, state) {
          return SizedBox(
            height: 60,
            child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              currentIndex: state.currentIndex,
              onTap: (index) {
                _bloc.add(UpdateNavigationIndex(newIndex: index));
              },
              backgroundColor: Colors.white,
              selectedFontSize: 12,
              showSelectedLabels: true,
              showUnselectedLabels: true,
              selectedIconTheme: const IconThemeData(color: Colors.white),
              selectedItemColor: AppTheme.mainColor,
              unselectedItemColor: Colors.black,
              selectedLabelStyle: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 12,
                color: Colors.black,
              ),
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  label: "الرئيسية",
                  icon: Image(
                    image: AssetImage(
                      Assets.icons.homePNG,
                    ),
                    width: 25,
                    // fit: BoxFit.fill,
                  ),
                  activeIcon: Image(
                    image: AssetImage(
                      Assets.icons.activeHomePNG,
                    ),
                    width: 25,
                  ),
                ),
                BottomNavigationBarItem(
                  label: "الاقسام",
                  icon: Image(
                    image: AssetImage(
                      Assets.icons.elementPNG,
                    ),
                    width: 25,
                  ),
                  activeIcon: Image(
                    image: AssetImage(
                      Assets.icons.activeFramePNG,
                    ),
                    width: 25,
                  ),
                ),
                BottomNavigationBarItem(
                  label: "العروض",
                  icon: Image(
                    image: AssetImage(
                      Assets.icons.discountShapePNG,
                    ),
                    width: 25,
                  ),
                  activeIcon: Image(
                    image: AssetImage(
                      Assets.icons.activeDiscountShapePNG,
                    ),
                    width: 25,
                  ),
                ),
                BottomNavigationBarItem(
                  label: "السلة",
                  icon: Image(
                    image: AssetImage(
                      Assets.icons.bagPNG,
                    ),
                    width: 25,
                  ),
                  activeIcon: Image(
                    image: AssetImage(
                      Assets.icons.activBagPNG,
                    ),
                    width: 25,
                  ),
                ),
                BottomNavigationBarItem(
                  label: "حسابي",
                  icon: Image(
                    image: AssetImage(
                      Assets.icons.userPNG,
                    ),
                    width: 25,
                  ),
                  activeIcon: Image(
                    image: AssetImage(
                      Assets.icons.activeUserPNG,
                    ),
                    width: 25,
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
