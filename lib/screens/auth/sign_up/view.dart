import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../assets.dart';
import '../../../generated/locale_keys.g.dart';
import '../../../helper/app_theme.dart';
import '../../../helper/btn.dart';
import '../../../helper/custom_text copy.dart';
import '../../../helper/text_form.dart';
import '../../../uti/routers/routers.gr.dart';

class SignUpView extends StatefulWidget {
  const SignUpView({super.key});

  @override
  State<SignUpView> createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();
  final passwordController = TextEditingController();
  bool isPass = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 60.h,
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    child: Image.asset(
                      Assets.icons.logoPNG,
                      width: 100.w,
                    ),
                  ),
                ),
                AppTheme.sizedBoxH,
                Center(
                  child: Text(
                    tr(LocaleKeys.Auth_addAcount),
                    style: AppTheme.titleWelcom,
                  ),
                ),
                AppTheme.sizedBoxH,
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Text(
                    tr(LocaleKeys.Auth_nameLabol),
                    style: AppTheme.hintText,
                  ),
                ),
                AppTheme.sizedBoxtextForm,
                txtField(
                  controller: nameController,
                  validator: (v) {
                    if (v!.isEmpty) {
                      return tr(LocaleKeys.Auth_nameValidator);
                    } else {
                      return null;
                    }
                  },
                  onSaved: (o) {},
                  hintText: LocaleKeys.Auth_nameLabol.tr(),
                  enabled: true,
                  obscureText: false,
                  textInputType: TextInputType.text,
                ),
                AppTheme.sizedBoxH,
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Text(
                    tr(LocaleKeys.Auth_Email),
                    style: AppTheme.hintText,
                  ),
                ),
                AppTheme.sizedBoxtextForm,
                txtField(
                  controller: emailController,
                  validator: (v) {
                    if (v!.isEmpty) {
                      return LocaleKeys.Auth_email_required.tr();
                    } else {
                      return null;
                    }
                  },
                  onSaved: (o) {},
                  hintText: LocaleKeys.Auth_Email.tr(),
                  enabled: true,
                  obscureText: false,
                  textInputType: TextInputType.emailAddress,
                ),
                AppTheme.sizedBoxH,
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Text(
                    tr(LocaleKeys.Auth_phone),
                    style: AppTheme.hintText,
                  ),
                ),
                AppTheme.sizedBoxtextForm,
                txtField(
                  controller: phoneController,
                  validator: (v) {
                    if (v!.isEmpty) {
                      return LocaleKeys.Auth_wrong_mobile_number.tr();
                    } else {
                      return null;
                    }
                  },
                  onSaved: (o) {},
                  hintText: LocaleKeys.Auth_phone.tr(),
                  enabled: true,
                  obscureText: false,
                  textInputType: TextInputType.number,
                ),
                AppTheme.sizedBoxH,
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Text(
                    tr(LocaleKeys.Auth_password),
                    style: AppTheme.hintText,
                  ),
                ),
                AppTheme.sizedBoxtextForm,
                txtFieldPass(
                  controller: passwordController,
                  validator: (v) {
                    if (v!.isEmpty) {
                      return LocaleKeys.Auth_wrong_password.tr();
                    } else {
                      return null;
                    }
                  },
                  suffix: InkWell(
                    onTap: () {
                      setState(() {
                        isPass = !isPass;
                      });
                    },
                    child: Icon(
                      isPass ? Icons.visibility_off : Icons.visibility,
                      color: const Color.fromARGB(255, 198, 190, 190),
                      size: 15,
                    ),
                  ),
                  onSaved: (o) {},
                  hintText: LocaleKeys.Auth_password.tr(),
                  enabled: true,
                  obscureText: isPass,
                  textInputType: TextInputType.visiblePassword,
                ),
                AppTheme.sizedBoxH,
                Btn(
                  txt: tr(LocaleKeys.Auth_login),
                  onTap: () {
                    // AutoRouter.of(context).push(NavigationViewRoute());
                    context.router.pushAndPopUntil(
                      const NavigationViewRoute(),
                      predicate: (_) => false,
                    );
                  },
                ),
                AppTheme.sizedBoxH,
                AuthTextAccount(
                  text: tr(LocaleKeys.Auth_Dont_hav_account),
                  title: tr(LocaleKeys.Auth_Create_an_account),
                  onTap: () {
                    // context.replaceRoute(SignUpViewRoute());
                    context.router.pushAndPopUntil(
                      const SignInRoute(),
                      predicate: (_) => false,
                    );
                    // AutoRouter.of(context).push(SignUpViewRoute());
                  },
                ),
                AppTheme.sizedBoxH,
              ],
            ),
          ),
        ),
      ),
    );
  }
}
