import 'package:auto_route/auto_route.dart';
import 'package:caramelbb/helper/btn.dart';
import 'package:caramelbb/uti/routers/routers.gr.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../assets.dart';
import '../../../generated/locale_keys.g.dart';
import '../../../helper/app_theme.dart';
import '../../../helper/custom_text copy.dart';
import '../../../helper/text_form.dart';

class SignIn extends StatefulWidget {
  const SignIn({super.key});

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final phoneController = TextEditingController();
  final passwordController = TextEditingController();
  bool isPass = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 60.h,
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    child: Image.asset(
                      Assets.icons.logoPNG,
                      width: 100.w,
                    ),
                  ),
                ),
                AppTheme.sizedBoxH,
                Center(
                  child: Text(
                    "سجّل دخولك",
                    style: AppTheme.titleWelcom,
                  ),
                ),
                AppTheme.sizedBoxH,
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Text(
                    tr(LocaleKeys.Auth_phone),
                    style: AppTheme.hintText,
                  ),
                ),
                AppTheme.sizedBoxtextForm,
                txtField(
                  controller: phoneController,
                  validator: (v) {
                    if (v!.isEmpty) {
                      return LocaleKeys.Auth_wrong_mobile_number.tr();
                    } else {
                      return null;
                    }
                  },
                  onSaved: (o) {},
                  hintText: LocaleKeys.Auth_phone.tr(),
                  enabled: true,
                  obscureText: false,
                  textInputType: TextInputType.number,
                ),
                AppTheme.sizedBoxH,
                Padding(
                  padding: const EdgeInsets.only(left: 15, right: 15),
                  child: Text(
                    tr(LocaleKeys.Auth_password),
                    style: AppTheme.hintText,
                  ),
                ),
                AppTheme.sizedBoxtextForm,
                txtFieldPass(
                  controller: passwordController,
                  validator: (v) {
                    if (v!.isEmpty) {
                      return LocaleKeys.Auth_wrong_password.tr();
                    } else {
                      return null;
                    }
                  },
                  suffix: InkWell(
                    onTap: () {
                      setState(() {
                        isPass = !isPass;
                      });
                    },
                    child: Icon(
                      isPass ? Icons.visibility_off : Icons.visibility,
                      color: const Color.fromARGB(255, 198, 190, 190),
                      size: 15,
                    ),
                  ),
                  onSaved: (o) {},
                  hintText: LocaleKeys.Auth_password.tr(),
                  enabled: true,
                  obscureText: isPass,
                  textInputType: TextInputType.visiblePassword,
                ),
                AppTheme.sizedBoxH,
                Btn(
                  txt: tr(LocaleKeys.Auth_login),
                  onTap: () {
                    context.router.pushAndPopUntil(
                      const NavigationViewRoute(),
                      predicate: (_) => false,
                    );
                  },
                ),
                AppTheme.sizedBoxH,
                AuthTextAccount(
                  text: tr(LocaleKeys.Auth_Dont_hav_account),
                  title: tr(LocaleKeys.Auth_Create_an_account),
                  onTap: () {
                    AutoRouter.of(context).push(SignUpViewRoute());
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
