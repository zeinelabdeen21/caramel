import 'package:auto_route/auto_route.dart';
import 'package:caramelbb/generated/locale_keys.g.dart';
import 'package:caramelbb/helper/btn.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../assets.dart';
import '../../../helper/app_theme.dart';
import '../../../uti/routers/routers.gr.dart';

class Welcome extends StatelessWidget {
  const Welcome({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                Assets.icons.framePNG,
                width: 130.w,
              ),
              AppTheme.sizedBoxH,
              Text(
                "سجلي دخولك لتتمتعي بعروض \nوخصومات لا محدودة",
                textAlign: TextAlign.center,
                style: AppTheme.titleWelcom,
              ),
              AppTheme.sizedBoxH,
              Btn(
                txt: LocaleKeys.Auth_login,
                onTap: () {
                  AutoRouter.of(context).push(SignInRoute());
                },
              ),
              Btn(
                txt: LocaleKeys.Auth_signUp,
                onTap: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}
