import 'package:caramelbb/screens/sections/bloc/bloc.dart';
import 'package:caramelbb/screens/sections/bloc/state.dart';
import 'package:caramelbb/screens/sections/widget/custom_section.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:caramelbb/screens/sections/bloc/event.dart';
import 'package:flutter/material.dart';
import 'package:kiwi/kiwi.dart';

import '../../helper/app_theme.dart';

class SectionsView extends StatefulWidget {
  const SectionsView({super.key});

  @override
  State<SectionsView> createState() => _SectionsViewState();
}

class _SectionsViewState extends State<SectionsView> {
  int? id;
  int? i;
  final NavigationRailBloc _bloc = KiwiContainer().resolve()
    ..add(UpdateNavigationRailIndex(index: 0, refresh: false));

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        BlocBuilder(
          bloc: _bloc,
          builder: (context, state) {
            if (state is NavigationRailSuccesses) {
              return NavigationRail(
                backgroundColor: Colors.white,
                selectedIndex: state.selectedIndex,
                labelType: NavigationRailLabelType.all,
                selectedIconTheme: const IconThemeData(color: Colors.black),
                selectedLabelTextStyle: TextStyle(
                  color: AppTheme.mainColor,
                  fontSize: 12,
                  height: 2,
                ),
                unselectedLabelTextStyle: const TextStyle(
                  color: Colors.black,
                  fontSize: 12,
                  height: 2,
                ),
                onDestinationSelected: (index) {
                  _bloc.add(
                      UpdateNavigationRailIndex(index: index, refresh: true));
                  print("😈😈😈😈😈😈😈😈😈😈😈😈😈😈 >>>> $index");
                },
                destinations: List.generate(5, (index) {
                  return NavigationRailDestination(
                    icon: Image.network(
                      "https://www.caramelbb.com/image/cache/catalog/icons/1656231921perfume-default.png",
                      width: 25,
                    ),
                    label: const Text(
                      "العناية",
                    ),
                  );
                }),
              );
            } else if (state is HomeStateFailed) {
              return Center(
                child: Text(state.msg),
              );
            } else {
              return Center(
                child: Container(
                  width: 30,
                  height: 30,
                  child: const CircularProgressIndicator(),
                ),
              );
            }
          },
        ),
        Expanded(
          child: BlocBuilder(
            bloc: _bloc,
            builder: (context, state) {
              if (state is NavigationRailStart) {
                return const Center(
                    child: SizedBox(
                        width: 30,
                        height: 30,
                        child: CircularProgressIndicator()));
              } else if (state is NavigationRailSuccesses) {
                return const Center(
                  child: CustomSctionBody(),
                  // child: Text('Selected 22 index: ${state.selectedIndex}'),
                );
              } else if (state is HomeStateFailed) {
                return Center(
                  child: Text(state.msg),
                );
              } else {
                return const Center(
                  child: SizedBox(
                    width: 30,
                    height: 30,
                    child: CircularProgressIndicator(),
                  ),
                );
              }
            },
          ),
        ),
      ],
    );
  }
}

// int selectedIndex = 0;

// int indexx = 0;
// var boardingController = PageController();
// List<Widget> screens = [
//   const CustomSctionBody(),
//   const CustomSctionBody(),
//   const CustomSctionBody(),
//   const CustomSctionBody(),
//   const CustomSctionBody(),
// ];
   // NavigationRail(
              // backgroundColor: Colors.white,
              // selectedIndex: selectedIndex,
              // labelType: NavigationRailLabelType.all,
              // selectedIconTheme: const IconThemeData(color: Colors.black),

              // selectedLabelTextStyle: TextStyle(
              //   color: AppTheme.mainColor,
              //   fontSize: 12,
              //   height: 2,
              // ),
              // unselectedLabelTextStyle: const TextStyle(
              //   color: Colors.black,
              //   fontSize: 12,
              //   height: 2,
              // ),

              //   // trailing: Container(
              //   //   height: 3,
              //   //   width: 25.w,
              //   //   color: Colors.black,
              //   // ),
              //   onDestinationSelected: (index) {
              //     setState(
              //       () {
              //         selectedIndex = index;
              //         indexx = index;
              //       },
              //     );
              //   },
              //   destinations: List.generate(5, (index) {
              //     return NavigationRailDestination(
              //       icon: Image.network(
              //         "https://www.caramelbb.com/image/cache/catalog/icons/1656231921perfume-default.png",
              //         width: 25,
              //       ),
              //       label: const Text(
              //         "العناية",
              //       ),
              //     );
              //   }),
              //   // destinations: [
              //   //   NavigationRailDestination(
              //   //       icon: Icon(Icons.home), label: Text("home")),
              //   //   NavigationRailDestination(
              //   //       icon: Icon(Icons.home), label: Text("home")),
              //   //   NavigationRailDestination(
              //   //       icon: Icon(Icons.home), label: Text("home")),
              //   //   NavigationRailDestination(
              //   //       icon: Icon(Icons.home), label: Text("home")),
              //   //   NavigationRailDestination(
              //   //       icon: Icon(Icons.home), label: Text("home")),
              //   //   NavigationRailDestination(
              //   //       icon: Icon(Icons.home), label: Text("home")),
              //   //   NavigationRailDestination(
              //   //       icon: Icon(Icons.home), label: Text("home")),
              //   //   NavigationRailDestination(
              //   //       icon: Icon(Icons.home), label: Text("home")),
              //   // ],
              // ),
              // const VerticalDivider(thickness: 1, width: 1),
              // Expanded(child: screens[indexx]),