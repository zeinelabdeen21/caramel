abstract class NavigationRailEvent {}

class UpdateNavigationRailIndex extends NavigationRailEvent {
  final int? index;
  final bool refresh;

  UpdateNavigationRailIndex({this.index, this.refresh = false});
}
