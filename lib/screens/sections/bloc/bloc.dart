import 'package:caramelbb/screens/sections/bloc/event.dart';
import 'package:caramelbb/screens/sections/bloc/state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class NavigationRailBloc extends Bloc<NavigationRailEvent, NavigationRailS> {
  // NavigationRailBloc() : super(NavigationRailState(0));
  NavigationRailBloc() : super(NavigationRailS()) {
    on<UpdateNavigationRailIndex>(_getData2);
  }

  // void _getData(
  //   NavigationRailEvent event,
  //   Emitter<UpdateNavigationRailIndex> emit,
  // ) async {
  //   if (event is UpdateNavigationRailIndex) {
  //     emit(NavigationRailState(event.index));
  //   }
  // }

  void _getData2(
    UpdateNavigationRailIndex event,
    Emitter<NavigationRailS> emit,
  ) async {
    emit(NavigationRailStart());
    emit(NavigationRailSuccesses(
        selectedIndex: event.index, refresh: event.refresh));
  }
}
