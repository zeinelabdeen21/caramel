// class NavigationRailState {
//   final int selectedIndex;

//   NavigationRailState(this.selectedIndex);
// }

class NavigationRailS {}

class NavigationRailStart extends NavigationRailS {}

class NavigationRailSuccesses extends NavigationRailS {
  final int? selectedIndex;
  late final bool? refresh;

  NavigationRailSuccesses({this.selectedIndex, this.refresh = false});
}

class HomeStateFailed extends NavigationRailS {
  String msg;
  int errType;
  HomeStateFailed({
    required this.errType,
    required this.msg,
  });
}
