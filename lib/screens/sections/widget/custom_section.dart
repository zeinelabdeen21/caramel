import 'package:caramelbb/helper/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomSctionBody extends StatefulWidget {
  const CustomSctionBody({super.key});

  @override
  State<CustomSctionBody> createState() => _CustomSctionBodyState();
}

class _CustomSctionBodyState extends State<CustomSctionBody> {
  List<String> list = [
    'الفرش والادوات',
    'الخدود',
    "الاضاءه",
    'الحواجب',
    'العيون',
    "الشفاه",
  ];
  List<String> title = ['الحواجب', 'العيون', "الشفاه", 'الوجه'];
  List<String> imagePath = [
    'https://img.pikbest.com/png-images/qiantu/cartoon-hand-drawn-cosmetics-png-elements_2583773.png!c1024wm0/compress/true/progressive/true/format/webp/fw/1024',
    'https://www.pngitem.com/pimgs/m/220-2208746_maquillaje-makeup-makeup-png-transparent-png.png',
    "https://freepngimg.com/thumb/makeup/166135-product-cosmetics-png-file-hd.png",
    'https://img.pikbest.com/png-images/qiantu/vector-hand-drawn-cartoon-cosmetics_2497164.png!c1024wm0/compress/true/progressive/true/format/webp/fw/1024',
  ];

  // late int w;

  late double h = 0;
  late bool expanded;
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.network(
          "https://image.freepik.com/free-vector/sale-banner-with-cosmetic-products-black-silk_107791-2095.jpg",
        ),
        Padding(
          padding:
              const EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 5),
          child: Container(
            padding:
                const EdgeInsets.only(left: 10, right: 10, top: 20, bottom: 20),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(8),
            ),
            child: const Text(
              "جميع المنتجات",
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
          ),
        ),
        Card(
          color: Colors.white,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: ListView.builder(
              shrinkWrap: true,
              physics: const BouncingScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                return ExpandableListView(title: list[index]);
              },
              itemCount: list.length,
            ),
          ),
        ),
      ],
    );
  }
}

class ExpandableListView extends StatefulWidget {
  ExpandableListView({
    Key? key,
    required this.title,
  }) : super(key: key);
  String title;

  @override
  _ExpandableListViewState createState() => new _ExpandableListViewState();
}

class _ExpandableListViewState extends State<ExpandableListView> {
  bool expandFlag = false;
  List<String> title = ['الحواجب', 'العيون', "الشفاه", 'الوجه'];
  List<String> imagePath = [
    'https://img.pikbest.com/png-images/qiantu/cartoon-hand-drawn-cosmetics-png-elements_2583773.png!c1024wm0/compress/true/progressive/true/format/webp/fw/1024',
    'https://www.pngitem.com/pimgs/m/220-2208746_maquillaje-makeup-makeup-png-transparent-png.png',
    "https://freepngimg.com/thumb/makeup/166135-product-cosmetics-png-file-hd.png",
    'https://img.pikbest.com/png-images/qiantu/vector-hand-drawn-cartoon-cosmetics_2497164.png!c1024wm0/compress/true/progressive/true/format/webp/fw/1024',
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 1.0),
      child: Column(
        children: <Widget>[
          InkWell(
            onTap: () {
              setState(
                () {
                  expandFlag = !expandFlag;
                },
              );
            },
            child: Container(
              color: Colors.white,
              padding: const EdgeInsets.symmetric(horizontal: 5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    widget.title,
                    style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                    ),
                  ),
                  Row(
                    children: [
                      expandFlag == true
                          ? Text(
                              "عرض الكل",
                              style: TextStyle(
                                color: AppTheme.mainColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 12,
                              ),
                            )
                          : const SizedBox(),
                      IconButton(
                          onPressed: () {
                            setState(
                              () {
                                expandFlag = !expandFlag;
                              },
                            );
                          },
                          icon: Icon(expandFlag == false
                              ? Icons.arrow_drop_down_sharp
                              : Icons.arrow_drop_up))
                    ],
                  ),
                ],
              ),
            ),
          ),
          ExpandableContainer(
              expanded: expandFlag,
              child: GridView.builder(
                padding: const EdgeInsets.all(15.0),
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                // crossAxisCount: size > 300 ? 2 : size > 500 ? 5 : size > 600 ? 8 : 9,

                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  // crossAxisCount: mmm(context),
                  mainAxisSpacing: 10.0,
                  crossAxisSpacing: 10.0,
                  childAspectRatio: 1 / 1,
                ),
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(
                        color: Colors.grey.shade300,
                        width: 0.5,
                      ),
                    ),
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(top: 5),
                          height: 25.h,
                          width: 25.w,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                            image: NetworkImage(
                              imagePath[index],
                            ),
                            fit: BoxFit.cover,
                          )),
                        ),
                        SizedBox(
                          height: 8.h,
                        ),
                        Text(
                          title[index],
                          style: const TextStyle(
                            color: Colors.black54,
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                          ),
                        )
                      ],
                    ),
                  );
                },
                itemCount: title.length,
              ))
        ],
      ),
    );
  }
}

class ExpandableContainer extends StatelessWidget {
  final bool expanded;
  final double collapsedHeight;
  final double expandedHeight;
  final Widget child;

  const ExpandableContainer({
    required this.child,
    this.collapsedHeight = 0.0,
    this.expandedHeight = 200.0,
    this.expanded = true,
  });

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return AnimatedContainer(
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeInCirc,
      width: screenWidth,
      height: expanded ? expandedHeight : collapsedHeight,
      child: Container(
        child: child,
        decoration: BoxDecoration(
            // borderRadius: BorderRadius.circular(20),
            border: Border.all(width: 1.0, color: Colors.grey.shade200)),
      ),
    );
  }
}
