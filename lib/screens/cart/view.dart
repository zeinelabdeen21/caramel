import 'package:caramelbb/helper/btn.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../generated/locale_keys.g.dart';
import '../../helper/app_theme.dart';
import '../../helper/text.dart';

class CartView extends StatefulWidget {
  const CartView({super.key});

  @override
  State<CartView> createState() => _CartViewState();
}

class _CartViewState extends State<CartView> {
  final List<Map> data =
      List.generate(10, (index) => {'quantity': 1, 'isSelected': false});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppTheme.backgroundScreen,
      floatingActionButton: Container(
        height: 110.h,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: AppTheme.mainColor.withOpacity(0.1),
              spreadRadius: 2,
              blurRadius: 3,
              offset: const Offset(0, -5), // changes position of shadow
            ),
          ],
          color: Colors.white,
          borderRadius: const BorderRadius.only(
            topRight: Radius.circular(15),
            topLeft: Radius.circular(15),
          ),
        ),
        width: double.infinity,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsetsDirectional.fromSTEB(30, 20, 30, 20),
              child: Row(
                children: [
                  const Text(
                    "الاجمالي :",
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                  const Spacer(),
                  Row(
                    children: [
                      const Text(
                        "170",
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w900,
                          color: Colors.red,
                        ),
                      ),
                      SizedBox(
                        width: 5.w,
                      ),
                      Text(
                        tr(LocaleKeys.BoxCatogry_RS),
                        style: const TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w900,
                          color: Colors.red,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const Spacer(),
            Btn(
              height: 40,
              txt: "اتمام الطلب",
              onTap: () {},
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: Padding(
        padding: const EdgeInsetsDirectional.fromSTEB(15, 20, 15, 20),
        child: ListView(
          children: [
            Container(
              padding: const EdgeInsetsDirectional.fromSTEB(15, 20, 15, 20),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.white,
              ),
              child: const Text(
                "عربة التسوق",
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            SizedBox(
              height: 15.h,
            ),
            ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: data.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(0, 7, 0, 7),
                  child: Container(
                    padding: const EdgeInsetsDirectional.fromSTEB(8, 5, 8, 5),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          child: Image.network(
                            "https://www.caramelbb.com/image/cache/catalog/products/diffuser%20/1675680481Diffuser-11-200x200.png",
                            fit: BoxFit.cover,
                          ),
                        ),
                        Expanded(
                          flex: 4,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  lengthTetx(
                                    nu: 30,
                                    txt:
                                        "سنيكرز برو ليزر مصنوع من الجلد الفائق من تشكيلة كيث هارينج أبيض/أسود",
                                    contaxt: context,
                                  ),
                                  const Spacer(),
                                  InkWell(
                                    onTap: () {},
                                    child: Container(
                                      width: 20,
                                      height: 20,
                                      decoration: BoxDecoration(
                                        color: AppTheme.mainColor,
                                        borderRadius: BorderRadius.circular(6),
                                      ),
                                      alignment: Alignment.center,
                                      child: const Icon(
                                        Icons.delete,
                                        size: 16,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 20.h,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      const Text(
                                        "170",
                                        style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w900,
                                          color: Colors.red,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 5.w,
                                      ),
                                      Text(
                                        tr(LocaleKeys.BoxCatogry_RS),
                                        style: const TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w900,
                                          color: Colors.red,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          setState(() {
                                            if (data[index]['quantity'] > 1) {
                                              data[index]['quantity']--;
                                            } else if (data[index]
                                                    ['quantity'] <=
                                                1) {
                                              data[index]['isSelected'] =
                                                  !data[index]['isSelected'];
                                            }
                                            // data[index]['isSelected'] = !data[index]['isSelected'];
                                          });
                                        },
                                        child: Container(
                                            width: 16,
                                            height: 16,
                                            decoration: BoxDecoration(
                                              color: AppTheme.mainColor,
                                              borderRadius:
                                                  BorderRadius.circular(4),
                                            ),
                                            alignment: Alignment.center,
                                            // trash_icon
                                            child: data[index]['quantity'] > 1
                                                ? const Icon(
                                                    Icons.remove,
                                                    size: 10,
                                                    color: Colors.white,
                                                  )
                                                : const Icon(
                                                    Icons.horizontal_rule,
                                                    size: 10,
                                                    color: Colors.white,
                                                  )),
                                      ),
                                      SizedBox(
                                        width: 5.w,
                                      ),
                                      Text(data[index]['quantity'].toString()),
                                      SizedBox(
                                        width: 5.w,
                                      ),
                                      InkWell(
                                        onTap: () {
                                          setState(() {
                                            data[index]['quantity']++;
                                            // data[index]['isSelected'] = !data[index]['isSelected'];
                                          });
                                        },
                                        child: Container(
                                          width: 16,
                                          height: 16,
                                          decoration: BoxDecoration(
                                            color: AppTheme.mainColor,
                                            borderRadius:
                                                BorderRadius.circular(4),
                                          ),
                                          alignment: Alignment.center,
                                          child: const Icon(
                                            Icons.add,
                                            size: 10,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
            SizedBox(
              height: 120.h,
            ),
          ],
        ),
      ),
    );
  }
}
