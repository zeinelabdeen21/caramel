import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../assets.dart';
import '../../../helper/app_theme.dart';

class BoxMune extends StatelessWidget {
  final dynamic ontap;
  final String icon;
  final String txt;
  const BoxMune({
    super.key,
    this.ontap,
    required this.icon,
    required this.txt,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: InkWell(
        onTap: ontap,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 12),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
          ),
          child: Row(
            children: [
              Image.asset(
                icon,
                width: 25,
              ),
              SizedBox(
                width: 8.w,
              ),
              Text(
                txt,
                style: AppTheme.title,
              ),
              const Spacer(),
              const Icon(
                Icons.arrow_forward_ios_outlined,
                size: 18,
                color: Colors.black,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
