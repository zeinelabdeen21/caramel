import 'package:caramelbb/helper/app_theme.dart';
import 'package:caramelbb/screens/my_profile/widget/widget_box_mune.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../assets.dart';
import '../../helper/btn.dart';

class MyProfile extends StatelessWidget {
  const MyProfile({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsetsDirectional.fromSTEB(15, 20, 15, 20),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsetsDirectional.fromSTEB(15, 20, 15, 20),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.white,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.all(5),
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.blue.shade300,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Image.asset(
                      Assets.icons.hackerPNG,
                      width: 80,
                    ),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text(
                        "مستخدم كارميل",
                        style: TextStyle(
                          fontSize: 13,
                          color: Colors.black,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Text(
                        "info@caramelbb.com",
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.grey,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 5.h,
            ),
            BoxMune(
              ontap: () {},
              icon: Assets.icons.truckFastPNG,
              txt: "تتبع الشحنة",
            ),
            BoxMune(
              ontap: () {},
              icon: Assets.icons.boxTickPNG,
              txt: "طلباتي",
            ),
            BoxMune(
              ontap: () {},
              icon: Assets.icons.iconUserPNG,
              txt: "الملف الشخصي",
            ),
            BoxMune(
              ontap: () {},
              icon: Assets.icons.lockPNG,
              txt: "تغيير كلمة المرور",
            ),
            BoxMune(
              ontap: () {},
              icon: Assets.icons.routingPNG,
              txt: "عناوين الشحن",
            ),
            BoxMune(
              ontap: () {},
              icon: Assets.icons.receiptItemPNG,
              txt: "طلبات الإرجاع",
            ),
            BoxMune(
              ontap: () {},
              icon: Assets.icons.dollarCirclePNG,
              txt: "الرصيد",
            ),
            BoxMune(
              ontap: () {},
              icon: Assets.icons.ticketStarPNG,
              txt: "نقاط المكافآت",
            ),
            BoxMune(
              ontap: () {},
              icon: Assets.icons.convertCardPNG,
              txt: "المدفوعات المتكررة",
            ),
            BoxMune(
              ontap: () {},
              icon: Assets.icons.smsTrackingPNG,
              txt: "القائمة البريدية",
            ),
            SizedBox(
              height: 15.h,
            ),
            BtnOut(
              onTap: () {},
              txt: "تسجيل الخروج",
            )
          ],
        ),
      ),
    );
  }
}
