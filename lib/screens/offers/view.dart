import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../generated/locale_keys.g.dart';
import '../../helper/app_theme.dart';
import '../../helper/text.dart';

class OffersView extends StatefulWidget {
  const OffersView({super.key});

  @override
  State<OffersView> createState() => _OffersViewState();
}

class _OffersViewState extends State<OffersView> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView(
        padding: const EdgeInsetsDirectional.fromSTEB(15, 20, 15, 20),
        children: [
          const Text(
            "العروض",
            style: TextStyle(
              fontSize: 14,
              color: Colors.black,
            ),
          ),
          SizedBox(
            height: 15.h,
          ),
          ...List.generate(5, (index) {
            return Padding(
              padding: const EdgeInsetsDirectional.fromSTEB(0, 7, 0, 7),
              child: Container(
                padding: const EdgeInsetsDirectional.fromSTEB(8, 5, 8, 5),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          lengthTetx(
                            nu: 30,
                            txt:
                                "سنيكرز برو ليزر مصنوع من الجلد الفائق من تشكيلة كيث هارينج أبيض/أسود",
                            contaxt: context,
                          ),
                          SizedBox(
                            height: 8.w,
                          ),
                          Row(
                            children: [
                              Row(
                                children: [
                                  const Text(
                                    "170",
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w900,
                                      color: Colors.red,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5.w,
                                  ),
                                  Text(
                                    tr(LocaleKeys.BoxCatogry_RS),
                                    style: const TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w900,
                                      color: Colors.red,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                width: 8.w,
                              ),
                              Row(
                                children: [
                                  const Text(
                                    '600.50',
                                    style: TextStyle(
                                      decoration: TextDecoration.lineThrough,
                                      decorationColor: Colors.grey,
                                      decorationThickness: 2,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5.w,
                                  ),
                                  Text(
                                    tr(LocaleKeys.BoxCatogry_RS),
                                    style: const TextStyle(
                                      decoration: TextDecoration.lineThrough,
                                      decorationColor: Colors.grey,
                                      decorationThickness: 2,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.grey,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 8.w,
                          ),
                          Row(
                            children: [
                              Container(
                                padding: const EdgeInsetsDirectional.fromSTEB(
                                    8, 10, 8, 10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  color:
                                      const Color(0xFFFF4949).withOpacity(0.12),
                                ),
                                child: Text(
                                  "خصم 50%",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w700,
                                    color: AppTheme.mainColor,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10.w,
                              ),
                              data[index]['isSelected'] == false
                                  ? InkWell(
                                      onTap: () {
                                        setState(() {
                                          data[index]['isSelected'] =
                                              !data[index]['isSelected'];
                                        });
                                      },
                                      child: Container(
                                        width: 30,
                                        height: 30,
                                        decoration: BoxDecoration(
                                          color: AppTheme.mainColor,
                                          borderRadius:
                                              BorderRadius.circular(6),
                                        ),
                                        alignment: Alignment.center,
                                        child: const Icon(
                                          Icons.shopping_cart,
                                          size: 16,
                                          color: Colors.white,
                                        ),
                                      ),
                                    )
                                  : Row(
                                      children: [
                                        InkWell(
                                          onTap: () {
                                            setState(() {
                                              if (data[index]['quantity'] > 1) {
                                                data[index]['quantity']--;
                                              } else if (data[index]
                                                      ['quantity'] <=
                                                  1) {
                                                data[index]['isSelected'] =
                                                    !data[index]['isSelected'];
                                              }
                                              // data[index]['isSelected'] = !data[index]['isSelected'];
                                            });
                                          },
                                          child: Container(
                                              width: 16,
                                              height: 16,
                                              decoration: BoxDecoration(
                                                color: AppTheme.mainColor,
                                                borderRadius:
                                                    BorderRadius.circular(4),
                                              ),
                                              alignment: Alignment.center,
                                              // trash_icon
                                              child: data[index]['quantity'] > 1
                                                  ? const Icon(
                                                      Icons.remove,
                                                      size: 10,
                                                      color: Colors.white,
                                                    )
                                                  : const Icon(
                                                      Icons.horizontal_rule,
                                                      size: 10,
                                                      color: Colors.white,
                                                    )),
                                        ),
                                        SizedBox(
                                          width: 5.w,
                                        ),
                                        Text(
                                            data[index]['quantity'].toString()),
                                        SizedBox(
                                          width: 5.w,
                                        ),
                                        InkWell(
                                          onTap: () {
                                            setState(() {
                                              data[index]['quantity']++;
                                              // data[index]['isSelected'] = !data[index]['isSelected'];
                                            });
                                          },
                                          child: Container(
                                            width: 16,
                                            height: 16,
                                            decoration: BoxDecoration(
                                              color: AppTheme.mainColor,
                                              borderRadius:
                                                  BorderRadius.circular(4),
                                            ),
                                            alignment: Alignment.center,
                                            child: const Icon(
                                              Icons.add,
                                              size: 10,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Image.network(
                        "https://www.caramelbb.com/image/cache/catalog/products/diffuser%20/1675680481Diffuser-11-200x200.png",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
        ],
      ),
    );
  }

  final List<Map> data =
      List.generate(10, (index) => {'quantity': 0, 'isSelected': false});
}
