import 'package:animate_do/animate_do.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../assets.dart';
import '../../uti/routers/routers.gr.dart';

class SplashView extends StatefulWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  const SplashView({super.key, required this.navigatorKey});

  @override
  State<SplashView> createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  _checkingData() async {
    Future.delayed(Duration(seconds: 3), () {
      // AutoRouter.of(context).push(NavigationViewRoute());
      context.router.pushAndPopUntil(
        const NavigationViewRoute(),
        predicate: (_) => false,
      );

      // Utils.manipulateSplashData(context);
    });
  }

  @override
  void initState() {
    _checkingData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElasticInUp(
          duration: const Duration(seconds: 2),
          child: Image.asset(
            Assets.icons.logoTowPNG,
            width: 250.w,
          ),
        ),
      ),
    );
  }
}
