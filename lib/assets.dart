
class Assets {
  Assets._();

  static final fonts = _AssetsFonts._();
  static final icons = _AssetsIcons._();
  static final lang = _AssetsLang._();

}

class _AssetsFonts {
  _AssetsFonts._();


  final jfFlatBoldTTF = 'assets/fonts/JF_Flat_bold.ttf';
  final jfFlatRegularTTF = 'assets/fonts/JF_Flat_regular.ttf';
}

class _AssetsIcons {
  _AssetsIcons._();


  final activBagPNG = 'assets/icons/activ-bag.png';
  final activeDiscountShapePNG = 'assets/icons/active-discount-shape.png';
  final activeUserPNG = 'assets/icons/active-user.png';
  final activeFramePNG = 'assets/icons/activeFrame.png';
  final activeHomePNG = 'assets/icons/activeHome.png';
  final avatarPNG = 'assets/icons/avatar.png';
  final bagPNG = 'assets/icons/bag.png';
  final boxTickPNG = 'assets/icons/box-tick.png';
  final convertCardPNG = 'assets/icons/convert-card.png';
  final discountShapePNG = 'assets/icons/discount-shape.png';
  final dollarCirclePNG = 'assets/icons/dollar-circle.png';
  final elementPNG = 'assets/icons/element.png';
  final framePNG = 'assets/icons/frame.png';
  final hackerPNG = 'assets/icons/hacker.png';
  final heartPNG = 'assets/icons/heart.png';
  final heartCPNG = 'assets/icons/heartC.png';
  final homePNG = 'assets/icons/home.png';
  final iconUserPNG = 'assets/icons/icon_user.png';
  final lockPNG = 'assets/icons/lock.png';
  final logoPNG = 'assets/icons/logo.png';
  final logoAppPNG = 'assets/icons/logo_app.png';
  final logoTowPNG = 'assets/icons/logo_tow.png';
  final receiptItemPNG = 'assets/icons/receipt-item.png';
  final routingPNG = 'assets/icons/routing.png';
  final searchPNG = 'assets/icons/search.png';
  final smsTrackingPNG = 'assets/icons/sms-tracking.png';
  final ticketStarPNG = 'assets/icons/ticket-star.png';
  final truckFastPNG = 'assets/icons/truck-fast.png';
  final userPNG = 'assets/icons/user.png';
}

class _AssetsLang {
  _AssetsLang._();


  final arJSON = 'assets/lang/ar.json';
  final enJSON = 'assets/lang/en.json';
}
