import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'uti/routers/routers.gr.dart';

// final GlobalKey<NavigatorState> navigator = GlobalKey<NavigatorState>();

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  final navigatorKey = GlobalKey<NavigatorState>();
  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(360, 690),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context, child) {
          return MaterialApp.router(
            // navigatorKey: navigator,
            localizationsDelegates: context.localizationDelegates,
            supportedLocales: context.supportedLocales,
            locale: context.locale,
            debugShowCheckedModeBanner: false,
            title: 'Flutter Demo',
            routerDelegate: _appRouter.delegate(
                initialRoutes: [SplashViewRoute(navigatorKey: navigatorKey)]),
            routeInformationParser: _appRouter.defaultRouteParser(),
            theme: ThemeData(
              scaffoldBackgroundColor: Colors.white,
              fontFamily: 'regular',
              primarySwatch: Colors.blue,
              backgroundColor: const Color(0xFFF7F7FF),
              appBarTheme: const AppBarTheme(
                iconTheme: IconThemeData(
                  color: Colors.black,
                  size: 25,
                ),
                elevation: 0.5,
                toolbarHeight: 75,
                centerTitle: false,
                backgroundColor: Colors.white,
                titleTextStyle: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
            // home: const SplashView(),
            builder: (ctx, child) {
              child = FlutterEasyLoading(child: child);
              return child;
            },
          );
        });
  }
}

class _Unfocus extends StatelessWidget {
  const _Unfocus({Key? key, this.child}) : super(key: key);

  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: child,
    );
  }
}
