// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const Auth_nameLabol = 'Auth.nameLabol';
  static const Auth_nameValidator = 'Auth.nameValidator';
  static const Auth_login = 'Auth.login';
  static const Auth_signUp = 'Auth.signUp';
  static const Auth_changePassword = 'Auth.changePassword';
  static const Auth_password = 'Auth.password';
  static const Auth_wrong_password = 'Auth.wrong_password';
  static const Auth_confirm_password = 'Auth.confirm_password';
  static const Auth_phone = 'Auth.phone';
  static const Auth_wrong_mobile_number = 'Auth.wrong_mobile_number';
  static const Auth_forgot_your_password = 'Auth.forgot_your_password';
  static const Auth_Dont_hav_account = 'Auth.Dont_hav_account';
  static const Auth_Create_an_account = 'Auth.Create_an_account';
  static const Auth_Email = 'Auth.Email';
  static const Auth_email_required = 'Auth.email_required';
  static const Auth_activate_the_account = 'Auth.activate_the_account';
  static const Auth_activation_code = 'Auth.activation_code';
  static const Auth_activation_code_phone_text = 'Auth.activation_code_phone_text';
  static const Auth_No_code_sent_yet = 'Auth.No_code_sent_yet';
  static const Auth_resend_the_code = 'Auth.resend_the_code';
  static const Auth_addAcount = 'Auth.addAcount';
  static const Auth = 'Auth';
  static const Btn_Confirmation = 'Btn.Confirmation';
  static const Btn_Login = 'Btn.Login';
  static const Btn_next = 'Btn.next';
  static const Btn = 'Btn';
  static const BoxCatogry_RS = 'BoxCatogry.RS';
  static const BoxCatogry = 'BoxCatogry';

}
