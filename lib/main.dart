import 'package:caramelbb/app.dart';
import 'package:caramelbb/helper/kiwi.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart' as lang;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await lang.EasyLocalization.ensureInitialized();
  KiwiInit();

  runApp(
    EasyLocalization(
      path: 'assets/lang',
      saveLocale: true,
      startLocale: const Locale('ar'),
      useFallbackTranslations: true,
      fallbackLocale: const Locale('ar'),
      supportedLocales: const [
        Locale('ar'),
        Locale('en'),
      ],
      child: const MyApp(),
    ),
  );
}

//  معدله
///Users/macos/Library/Android/sdk