import 'package:auto_route/auto_route.dart';

import '../../screens/auth/sign_in/view.dart';
import '../../screens/auth/sign_up/view.dart';
import '../../screens/auth/welcome/view.dart';
import '../../screens/home/view.dart';
import '../../screens/navigation/view.dart';
import '../../screens/splash/view.dart';

@AdaptiveAutoRouter(routes: <AutoRoute>[
  CustomRoute(
      page: SplashView,
      initial: true,
      transitionsBuilder: TransitionsBuilders.fadeIn,
      durationInMilliseconds: 500),
  CustomRoute(
      page: Welcome,
      transitionsBuilder: TransitionsBuilders.fadeIn,
      durationInMilliseconds: 500),
  CustomRoute(
      page: SignIn,
      transitionsBuilder: TransitionsBuilders.fadeIn,
      durationInMilliseconds: 500),
  CustomRoute(
      page: SignUpView,
      transitionsBuilder: TransitionsBuilders.fadeIn,
      durationInMilliseconds: 500),
  CustomRoute(
      page: NavigationView,
      transitionsBuilder: TransitionsBuilders.fadeIn,
      durationInMilliseconds: 500),
  CustomRoute(
      page: HomeView,
      transitionsBuilder: TransitionsBuilders.fadeIn,
      durationInMilliseconds: 500),
])
class $AppRouter {}
