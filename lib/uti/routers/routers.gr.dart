// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i7;
import 'package:caramelbb/screens/auth/sign_in/view.dart' as _i3;
import 'package:caramelbb/screens/auth/sign_up/view.dart' as _i4;
import 'package:caramelbb/screens/auth/welcome/view.dart' as _i2;
import 'package:caramelbb/screens/home/view.dart' as _i6;
import 'package:caramelbb/screens/navigation/view.dart' as _i5;
import 'package:caramelbb/screens/splash/view.dart' as _i1;
import 'package:flutter/material.dart' as _i8;

class AppRouter extends _i7.RootStackRouter {
  AppRouter([_i8.GlobalKey<_i8.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i7.PageFactory> pagesMap = {
    SplashViewRoute.name: (routeData) {
      final args = routeData.argsAs<SplashViewRouteArgs>();
      return _i7.CustomPage<dynamic>(
        routeData: routeData,
        child: _i1.SplashView(
          key: args.key,
          navigatorKey: args.navigatorKey,
        ),
        transitionsBuilder: _i7.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 500,
        opaque: true,
        barrierDismissible: false,
      );
    },
    WelcomeRoute.name: (routeData) {
      return _i7.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i2.Welcome(),
        transitionsBuilder: _i7.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 500,
        opaque: true,
        barrierDismissible: false,
      );
    },
    SignInRoute.name: (routeData) {
      return _i7.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i3.SignIn(),
        transitionsBuilder: _i7.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 500,
        opaque: true,
        barrierDismissible: false,
      );
    },
    SignUpViewRoute.name: (routeData) {
      return _i7.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i4.SignUpView(),
        transitionsBuilder: _i7.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 500,
        opaque: true,
        barrierDismissible: false,
      );
    },
    NavigationViewRoute.name: (routeData) {
      return _i7.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i5.NavigationView(),
        transitionsBuilder: _i7.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 500,
        opaque: true,
        barrierDismissible: false,
      );
    },
    HomeViewRoute.name: (routeData) {
      return _i7.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i6.HomeView(),
        transitionsBuilder: _i7.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 500,
        opaque: true,
        barrierDismissible: false,
      );
    },
  };

  @override
  List<_i7.RouteConfig> get routes => [
        _i7.RouteConfig(
          SplashViewRoute.name,
          path: '/',
        ),
        _i7.RouteConfig(
          WelcomeRoute.name,
          path: '/Welcome',
        ),
        _i7.RouteConfig(
          SignInRoute.name,
          path: '/sign-in',
        ),
        _i7.RouteConfig(
          SignUpViewRoute.name,
          path: '/sign-up-view',
        ),
        _i7.RouteConfig(
          NavigationViewRoute.name,
          path: '/navigation-view',
        ),
        _i7.RouteConfig(
          HomeViewRoute.name,
          path: '/home-view',
        ),
      ];
}

/// generated route for
/// [_i1.SplashView]
class SplashViewRoute extends _i7.PageRouteInfo<SplashViewRouteArgs> {
  SplashViewRoute({
    _i8.Key? key,
    required _i8.GlobalKey<_i8.NavigatorState> navigatorKey,
  }) : super(
          SplashViewRoute.name,
          path: '/',
          args: SplashViewRouteArgs(
            key: key,
            navigatorKey: navigatorKey,
          ),
        );

  static const String name = 'SplashViewRoute';
}

class SplashViewRouteArgs {
  const SplashViewRouteArgs({
    this.key,
    required this.navigatorKey,
  });

  final _i8.Key? key;

  final _i8.GlobalKey<_i8.NavigatorState> navigatorKey;

  @override
  String toString() {
    return 'SplashViewRouteArgs{key: $key, navigatorKey: $navigatorKey}';
  }
}

/// generated route for
/// [_i2.Welcome]
class WelcomeRoute extends _i7.PageRouteInfo<void> {
  const WelcomeRoute()
      : super(
          WelcomeRoute.name,
          path: '/Welcome',
        );

  static const String name = 'WelcomeRoute';
}

/// generated route for
/// [_i3.SignIn]
class SignInRoute extends _i7.PageRouteInfo<void> {
  const SignInRoute()
      : super(
          SignInRoute.name,
          path: '/sign-in',
        );

  static const String name = 'SignInRoute';
}

/// generated route for
/// [_i4.SignUpView]
class SignUpViewRoute extends _i7.PageRouteInfo<void> {
  const SignUpViewRoute()
      : super(
          SignUpViewRoute.name,
          path: '/sign-up-view',
        );

  static const String name = 'SignUpViewRoute';
}

/// generated route for
/// [_i5.NavigationView]
class NavigationViewRoute extends _i7.PageRouteInfo<void> {
  const NavigationViewRoute()
      : super(
          NavigationViewRoute.name,
          path: '/navigation-view',
        );

  static const String name = 'NavigationViewRoute';
}

/// generated route for
/// [_i6.HomeView]
class HomeViewRoute extends _i7.PageRouteInfo<void> {
  const HomeViewRoute()
      : super(
          HomeViewRoute.name,
          path: '/home-view',
        );

  static const String name = 'HomeViewRoute';
}
